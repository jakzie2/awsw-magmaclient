from modloader import modast, modinfo
from modloader.modclass import Mod, loadable_mod

@loadable_mod
class AWSWMod(Mod):
    name = "MagmaClient"
    version = "0.0.1"
    author = "Jakzie"
    
    def mod_load(self):
        modast.call_hook(modast.find_label("begingame"), modast.find_label("jz_magma_client_init"))
        modast.call_hook(modast.find_label("seccont"), modast.find_label("jz_magma_client_init"))
    
    def mod_complete(self):
        pass
