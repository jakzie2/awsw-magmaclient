init python:
    jz_magma_client_denied = False

label jz_magma_client_init:
    if jz_magma_client_denied:
        return
    
    menu:
        "Launch MagmaClient.":
            pass
        "Play normally.":
            $ jz_magma_client_denied = True
            return
    
    python in jz_magma_client:
        command_cache = []
        server_ip = tuple()
        
        def send_request(content):
            import httplib, json
            try:
                conn = httplib.HTTPConnection(server_ip[0], int(server_ip[1]))
                conn.request("POST", "/", json.dumps(content))
                response = conn.getresponse().read()
                response_json = json.loads(response)
                conn.close()
            except Exception:
                return None
            else:
                return response_json
        
        def send_func(func_name, **kwargs):
            return send_request({"awsw_magma_client": {"func": func_name, "params": kwargs}})
        
        def check():
            response_json = send_func("check")
            if response_json is None:
                return None
            if "name" not in response_json:
                return None
            return response_json
        
        def send_info(data):
            send_func("info_data", **data)
        
        def fetch_commands():
            response_json = send_func("fetch_commands")
            if response_json is None:
                return
            if "commands" not in response_json:
                return
            for command in response_json["commands"]:
                command_cache.append(command)
        
        def confirm(cmd_id):
            send_func("confirm", cmd_id=cmd_id)
        
        def command_response(command, response):
            send_func("cmd_response", cmd_id=command["id"], cmd_name=command["name"], response=response)
        
        def command_error(command, error_name, **kwargs):
            send_func("cmd_error", cmd_id=command["id"], cmd_name=command["name"], error_name=error_name, params=kwargs)
        
        def next_command():
            if len(command_cache) <= 1:
                fetch_commands()
            
            if len(command_cache) == 0:
                return None
            
            command = command_cache[0]
            del command_cache[0]
            
            return command
        
        def check_version(min_ver):
            from modloader import modinfo
            min_ver_spl = min_ver.split(".")
            ver_spl = modinfo.get_mod("MagmaClient").version.split(".")
            
            return int(min_ver_spl[0]) * 1000000 + int(min_ver_spl[1]) * 1000 + int(min_ver_spl[2]) <= int(ver_spl[0]) * 1000000 + int(ver_spl[1]) * 1000 + int(ver_spl[2])
            
            
    
    scene o at Pan((0, 250), (0, 250), 0.1) with dissolvemed
    
    python:
        def jz_magma_client_init_f():
            ip_str = renpy.input(_("Enter server IP and port:"), _("localhost:8042"))
            jz_magma_client.server_ip = tuple(ip_str.split(":", 1))
            
            while True:
                info = jz_magma_client.check()
                if info is None:
                    renpy.say("MagmaClient", "Cannot find any server to connect to. Click to try again.")
                    renpy.pause(1.0)
                elif not jz_magma_client.check_version(info["min_client_version"]):
                    renpy.say("MagmaClient", "Cannot connect to " + info["name"] + ", because your MagmaClient is outdated. Please update to a new version.")
                    renpy.pause(1.0)
                else:
                    characters = {}
                    transforms = []
                    for k, v in globals().items():
                        if isinstance(v, renpy.character.ADVCharacter):
                            characters[k] = v.image_tag
                        elif isinstance(v, renpy.display.transform.Transform) and len(v.parameters.parameters) == 0:
                            transforms.append(k)
                    images = [" ".join(k) for k in renpy.display.image.images.keys()]
                    jz_magma_client.send_info({"player": {"name": player_name}, "characters": characters, "images": images, "transforms": transforms})
                    renpy.say("MagmaClient", "Connected to: " + info["name"] + " " + info["version"])
                    break
        
        jz_magma_client_init_f()
    
    jump jz_magma_client_run

label jz_magma_client_run:
    python:
        def jz_magma_client_run_f():
            command = jz_magma_client.next_command()
        
            if command is None:
                renpy.pause(1.0)
                return
            
            jz_magma_client.confirm(command["id"])
            
            if command["name"] == "say":
                who = command["params"]["who"]
                what_list = command["params"]["what"]
                if who == "n":
                    _window_show()
                    for what in what_list:
                        n(what)
                    _window_hide()
                    nvl_clear()
                elif who in globals():
                    character = globals()[who]
                    if isinstance(character, renpy.character.ADVCharacter):
                        for what in what_list:
                            renpy.say(character, what)
                    else:
                        jz_magma_client.command_error(command, "key_error", key=who)
                else:
                    jz_magma_client.command_error(command, "key_error", key=who)
                    
            elif command["name"] == "show":
                image = command["params"]["image"]
                at = command["params"]["at"]
                at_spl = at.split()
                if len(at_spl) == 2 and (at_spl[0] + at_spl[1]).replace(".","").isdigit():
                    renpy.show(image, at_list=[Position(xpos=float(at_spl[0]), ypos=float(at_spl[1]))])
                    renpy.with_statement(dissolve)
                elif at in globals():
                    at_obj = globals()[at]
                    if isinstance(at_obj, renpy.display.transform.Transform) and len(at_obj.parameters.parameters) == 0:
                        renpy.show(image, at_list=[at_obj])
                        renpy.with_statement(dissolve)
                    else:
                        jz_magma_client.command_error(command, "key_error", key=at)
                else:
                    jz_magma_client.command_error(command, "key_error", key=at)
            elif command["name"] == "hide":
                image = command["params"]["image"]
                renpy.hide(image)
                renpy.with_statement(dissolve)
            elif command["name"] == "scene":
                image = command["params"]["image"]
                renpy.scene()
                renpy.show(image)
                renpy.with_statement(dissolve)
            elif command["name"] == "input":
                text = command["params"]["text"]
                jz_magma_client.command_response(command, renpy.input(_(text)))
            elif command["name"] == "menu":
                choices = command["params"]["choices"]
                jz_magma_client.command_response(command, renpy.display_menu([(k, k) for k in choices]))
            elif command["name"] == "play":
                filename = command["params"]["filename"]
                channel = command["params"]["channel"]
                renpy.audio.music.play(filename, channel, fadein=1.0)
            elif command["name"] == "stop":
                channel = command["params"]["channel"]
                renpy.audio.music.stop(channel, fadeout=1.0)
        
        jz_magma_client_run_f()
    
    jump jz_magma_client_run
