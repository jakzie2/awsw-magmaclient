# AwSW MagmaClient

MagmaClient is a HTTP client for Angels with Scaly Wings, which allows the player to connect to HTTP server and recieve dialogues and scenes from it. Its main purpose is to allow other modders to create apps, which can send commands to the game, either locally or remotely.

This mod is currently in early alpha state, therefore there may be some bugs and issues.

If you want to see an app which uses MagmaClient, check out [DiscordHook](https://gitlab.com/jakzie2/awsw-discordhook/-/wikis/home).

## Launching the client

After putting the mod into your game, you can launch MagmaClient by pressing Start in the main menu. Then a menu appears, asking you if you want to launch the client or play normally. If you're using a fresh save, the menu appears only after you enter your character name.

## Making your own server

If you want to make your own server for MagmaClient, you're free to do it using [this documentation](https://gitlab.com/jakzie2/awsw-magmaclient/-/wikis/home).
